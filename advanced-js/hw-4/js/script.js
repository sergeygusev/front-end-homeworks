"use strict"

async function getItems(url) {
    let response = await fetch(url);
    return await response.json();
}

let films = getItems("https://swapi.dev/api/films");

films
    .then(data => {
        let filmArray = data.results;
        let filmsContainer = document.querySelector(".films");

        filmArray.map(elem => {
        let filmItem = document.createElement("div");
        filmItem.classList.add("film__item");
        filmItem.setAttribute("id", elem.episode_id);
        filmItem.innerHTML = '<strong>Episode #' + `${elem.episode_id} / ${elem.title}` + '</strong><p><em>' + elem.opening_crawl + '</em></p>';
        filmsContainer.append(filmItem);
    });

    return filmArray;
})
    .then(data => {
        data.map(async elem => {
            const promises = elem.characters.map(async el => {
                return getItems(el);
            });

            let result = await Promise.all(promises);

            let characterList = document.createElement("ul");

            result.map(el => {
                let characterItem = document.createElement("li");
                characterItem.classList.add("character__item");
                characterItem.innerText = el.name;
                characterList.append(characterItem);
            })

            document.getElementById(elem.episode_id).append(characterList);
        });
    });











