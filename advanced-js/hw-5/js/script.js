"use strict"

let btn = document.querySelector(".send");

async function getData(url) {
    let response = await fetch(url);
    return await response.json();
}

btn.addEventListener('click', () => {
    let ul = document.querySelector("ul");
    if (null !== ul) {
        ul.remove();
    }

    let ip = getData("https://api.ipify.org/?format=json");
    ip
        .then(data => {
        return data.ip;
    })
        .then(data => {
            let response = getData(`http://ip-api.com/json/${data}`);
            response
                .then(data => {
                    let list = document.createElement('ul');

                    let country = document.createElement('li');
                    country.innerText = data.country;
                    list.append(country);

                    let regionName = document.createElement('li');
                    regionName.innerText = data.regionName;
                    list.append(regionName);

                    let city = document.createElement('li');
                    city.innerText = data.city;
                    list.append(city);

                    btn.after(list);
                })
        })

})











