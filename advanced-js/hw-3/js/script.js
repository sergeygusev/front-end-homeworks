"use strict"

class Employee {

    name = null;
    age = null;
    salary = null;

    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get getName() {
        return this.name;
    }

    set setName(name) {
        this.name = name;
    }

    get getAge() {
        return this.age;
    }

    set setAge(age) {
        this.age = age;
    }

    get getSalary() {
        return this.salary;
    }

    set setSalary(salary) {
        this.salary = salary;
    }
}

class Programmer extends Employee {

    lang = null;

    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get getSalary() {
        return this.salary * 3;
    }
}

let programmerOne = new Programmer("John", 20, 1000, "en");
let programmerTwo = new Programmer("Mike", 38, 1500, "fr");
let programmerThree = new Programmer("Ann", 25, 1200, "ru");

console.log(programmerOne);
console.log(programmerTwo);
console.log(programmerThree);

console.log(programmerOne.getSalary);
console.log(programmerTwo.getSalary);
console.log(programmerThree.getSalary);


