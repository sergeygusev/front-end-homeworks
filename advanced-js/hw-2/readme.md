# Ответ на теоретический вопрос
#### Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch.

* try {
    test;
} catch (e) {
    alert(e);
}

* try {
    let obj = {
        name,
        age
    }
    if (!role) {
        throw new Error("Role is undefined");
    }
} catch (e) {
    console.log(e);
}