"use strict"

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

let list = document.createElement('ul');

function validate(obj) {
    const { author, name, price } = obj;

    try {
        if (!author) {
            throw new Error("Должно быть поле Автор");
        } else if (!name) {
            throw new Error("Должно быть поле Название");
        } else if (!price) {
            throw new Error("Должно быть поле Цена");
        }
    } catch (e) {
        console.log(e.message);
        return false;
    }

    return `Автор: ${author}, Название: ${name}, Цена: ${price}`;
}

books.map(elem => {
    if (validate(elem)) {
        let item = document.createElement('li');
        item.innerHTML = validate(elem);
        list.append(item);
    }
});

let rootDiv = document.getElementById('root');

rootDiv.append(list);


