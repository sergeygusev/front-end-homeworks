"use strict"

let numberOne = "",
    numberTwo = "",
    operator = "";

while (numberValidate(numberOne)) {
    numberOne = prompt("Введите первое число", `${numberOne}`);
}

while (numberValidate(numberTwo)) {
    numberTwo = prompt("Введите второе число", `${numberTwo}`);
}

while (operatorValidate(operator)) {
    operator = prompt("Введите оператор", `${operator}`);
}

function operation(par1, par2, par3) {
    let result;

    switch (par3) {
        case "+":
           result = par1 + par2;
           break;
        case "-":
            result = par1 - par2;
            break;
        case "*":
            result = par1 * par2;
            break;
        case "/":
            result = par1 / par2;
            break;
        default:
            break;
    }

    return result;
}

function numberValidate(number) {
    return number.length === 0 || isNaN(+number);
}

function operatorValidate(operator) {
    return (
        operator !== "+" &&
        operator !== "-" &&
        operator !== "*" &&
        operator !== "/"
    );
}

console.log(operation(+numberOne, +numberTwo, operator));




