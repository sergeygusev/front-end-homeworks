"use strict"

function createNewUser() {

    let birthDay = prompt("Введите Вашу дату рождения", "dd.mm.yyyy");

    return {
        firstName: prompt("Введите Ваше имя", ""),
        lastName: prompt("Введите Вашу фамилию", ""),
        getLogin: function () {
            return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
        },
        getAge: function () {
            let splitData = birthDay.split('.');
            let birthdayYear = splitData[2];
            let birthdayMonth = splitData[1];
            let birthdayDay = splitData[0];

            let dateObj = new Date();

            let currentYear = dateObj.getFullYear();
            let currentMonth = dateObj.getMonth() + 1;
            let currentDay = dateObj.getDate();

            if (currentDay < birthdayDay && currentMonth > birthdayMonth)
                return currentYear - birthdayYear;

            return currentYear - 1 - birthdayYear;
        },
        getPassword: function () {
            return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.getAge()}`
        }
    }
}

const newUser = createNewUser();

console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());





