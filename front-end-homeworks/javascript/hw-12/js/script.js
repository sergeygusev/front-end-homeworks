"use strict"

let image = document.querySelectorAll('.image-to-show');
let imageWrapper = document.querySelectorAll('.images-wrapper');
let stop = document.createElement('button');
let start = document.createElement('button');
let i = 1;
let clear = false;

stop.innerText = "Прекратить";
stop.classList.add('stop');
imageWrapper[0].after(stop);

start.innerText = "Возобновить";
start.classList.add('start');
stop.after(start);

function startSlider() {
    let timerId = setInterval(() => {
        image.forEach(e => {
            e.style.display = 'none';
        });

        i = i === 4 ? 0 : i;

        image[i].style.display = 'block';
        i++
    }, 10000);

    stop.addEventListener('click', () => {
        clear = true;
        clearInterval(timerId);
    });

    start.addEventListener('click', () => {
        if (clear) {
            startSlider(timerId);
            clear = false;
        }
    });
}

startSlider();







