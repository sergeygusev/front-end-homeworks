"use strict"

let number = "";

while (isNaN(+number) || +number == 0 || !Number.isInteger(number)) {

    number = +prompt("Введите число", "");
}

if (number < 5 && number > 0) {
    console.log('Sorry, no numbers');
} else {
    for (let i = 0; i <= number; i++) {
        if (i === 0 || i % 5 !== 0)
            continue;

        console.log(i);
    }
}

if (number > -5 && number < 0) {
    console.log('Sorry, no numbers');
} else {
    for (let i = 0; i >= number; i--) {
        if (i === 0 || i % 5 !== 0)
            continue;

        console.log(i);
    }
}





