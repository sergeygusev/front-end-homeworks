"use strict"

function userReport() {
    const user = createUser();
    userGrade(user);
    userTabel(user.tabel);
}

function createUser() {
    let name,
        lastName;

    name = prompt("Введите Ваше имя", "");
    lastName = prompt("Введите Вашу фамилию", "");

    return {
        name: name,
        lastName: lastName
    }
}

function userGrade(object) {
    object.tabel = {};

    while (true) {
        let subject = prompt("Введите название предмета", "");

        if (!subject)
            break;

        let grade = +prompt("Оценка", "");

        object.tabel[subject] = grade;
    }
}

function userTabel(tabel) {
    let negative = false,
        summ = 0,
        count = 0;

    for (let key in tabel) {

        if (tabel[key] < 4) {
            console.log(`Студент переведен на следующий курс`);
            negative = true;
            break;
        }

        summ += tabel[key];
        count++;
    }

    if (!negative)
        console.log(`Студенту назначена стипендия`);
}

userReport();







