"use strict"

let userName = "",
    userAge = "";

while (userName.length == 0) {
    // Спрашиваем у пользователя его имя
    userName = prompt("Как вас зовут?", "");
}

while (isNaN(+userAge) || +userAge == 0) {
    // Спрашиваем у пользователя его возраст
    userAge = prompt("Какой ваш возраст?", `${userAge}`);
}

if (userAge < 18) {

    // Если возраст меньше 18 лет
    alert("You are not allowed to visit this website");

} else if (18 <= userAge && userAge <= 22) {

    // Если возраст меньше 18 и больше либо равно 22
    let agree = confirm("Are you sure you want to continue?");

    if (agree) {
        alert(`Welcome, ${userName}!`);
    } else {
        alert("You are not allowed to visit this website");
    }

} else {

    // Если возраст больше 22 лет
    alert(`Welcome, ${userName}!`);
}



