"use strict"

let input = document.querySelector('.input-price');
let form = document.querySelector('form');

function messageRemove(mess) {
    if (mess)
        mess.remove();
}

document.body.addEventListener('click', (e) => {

    let message = document.querySelector('.message');
    let error = document.querySelector('.error');
    let value = input.value;

    if (e.target.className.includes("input-price")) {
        input.classList.add('green');
    } else if (e.target.className === "close") {
        messageRemove(message);

        input.value = null;
    } else {
        input.classList.remove('green');
        input.classList.add('green-text');

        if (+value < 0) {
            messageRemove(message);
            input.classList.add('red');
            if (!error) {
                form.insertAdjacentHTML("beforeend", "<span class='error'>Please enter correct price</span>");
            }
        } else if (e.target.className === "wrapper" && +value > 0) {
            messageRemove(error);
            input.classList.remove('red');
            if (!message)
                form.insertAdjacentHTML("afterbegin", "<span class='message'>Текущая цена: " + value + "<div class='close'>[X]</div></span>");
        }
    }

});


