"use strict"

let drawCircle = document.getElementById('drawCircle');
let square = document.createElement("div");
square.classList.add("square");

drawCircle.addEventListener('click', () => {
    let getDiameter = +prompt("Диаметр круга", "");
    square.style.height = getDiameter + "px";
    square.style.width = getDiameter + "px";
    if (getDiameter > 0) {
        drawCircle.before(square);
    }
})

let draw = document.getElementById('draw');

function randomBg() {
    let r = Math.floor(Math.random() * 256);
    let g = Math.floor(Math.random() * 256);
    let b = Math.floor(Math.random() * 256);

    return `rgb(${r}, ${g}, ${b})`;
}

draw.addEventListener('click', () => {
    document.body.innerHTML = "";

    for (let i = 0; i < 100; i++) {
        let circle = document.createElement('div');

        circle.classList.add('circle');
        circle.setAttribute("id", i);
        circle.style.background = randomBg();

        document.body.append(circle);

        circle.addEventListener('click', (el) => {
            let bubbles = document.querySelectorAll('.circle');
            bubbles[i].remove();
            console.log(i);
        });
    }
});



