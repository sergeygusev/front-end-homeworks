'use strict'

$(document).ready(function () {

    $(".most_popular_posts").on('click', function () {

        $(this).text($("#most_popular_posts").is(':visible') ? 'Показать' : 'Спрятать');

        $("#most_popular_posts").slideToggle();
    })

    $(window).scroll(function () {
        if (pageYOffset > $(window).height()) {
            $(".up").show();
        } else {
            $(".up").hide();
        }
    })

    $(".up").on('click', function () {
        $('html, body').stop().animate({
            scrollTop: 0
        }, 800);
    })

    $('a[href*=#]').bind("click", function(e){
        let anchor = $(this);
        $('a').css({
            textDecoration: 'none'
        })
        anchor.css({
            textDecoration: 'underline'
        })
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top - 86
        }, 800);
        e.preventDefault();
    });
    return false;
})