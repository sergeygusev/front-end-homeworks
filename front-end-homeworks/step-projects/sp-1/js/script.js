"use strict"

let servicesTab = document.querySelectorAll(".our-services-tabs-title");
let servicesContent = document.querySelector('.our-services-tabs-content');

tabChecker(servicesTab, servicesContent, 'active');

let ourAmazingTab = document.querySelectorAll(".our-amazing-work-tabs-title");
let ourAmazingContent = document.querySelector('.our-amazing-work-tabs-content');

tabChecker(ourAmazingTab, ourAmazingContent, 'work-active');

function tabChecker(tab, tabContent, tabActive) {
    tab.forEach((el, index) => {
        if (el.classList.contains(tabActive)) {
            if (tabActive !== 'work-active')
                tabContent.children[index].style.display = 'block';
            else {

            }
        }
        el.addEventListener('click', event => {
            let active = document.querySelector(`.${tabActive}`);
            active.classList.remove(tabActive);
            event.target.classList.add(tabActive);
            let contentItem = tabContent.children;

            Array.from(contentItem).forEach(item => {
                let dataAttribute = el.innerText.toLowerCase().replace(/ /g,"-");

                if (dataAttribute === 'all') {
                    Array.from(contentItem).forEach(e => {
                        e.style.display = 'block';
                    })
                } else {
                    if (dataAttribute === item.dataset.attr) {
                        item.style.display = 'block';
                    } else
                        item.style.display = 'none';
                }
            })
        })
    });
}

let ourServicesImages = document.querySelector('.our-amazing-work-tabs-content');









// ourServicesImages.addEventListener('mouseover', (e) => {
//     if (e.target.nodeName === "IMG") {
//         e.target.parentNode.insertAdjacentHTML("afterbegin", "<div class='our-amazing-work-img-hover'></div>")
//     }
// });
//
// ourServicesImages.addEventListener('mouseleave', (e) => {
//
//     let hoverDiv = document.querySelectorAll('.our-amazing-work-img-hover');
//
//     hoverDiv.forEach(function(elem){
//         console.log(e);
//         elem.parentNode.removeChild(elem);
//     });
//
//     if (e.target.nodeName === "IMG") {
//         e.target.parentNode.insertAdjacentHTML("afterbegin", "<div class='our-amazing-work-img-hover'><div class='our-amazing-work-img-hover-link-wrap'><a class='our-amazing-work-img-hover-link' href='#'></a><a class='our-amazing-work-img-hover-link' href='#'></a></div><p class='our-amazing-work-img-hover-title'>creative design</p><p class='our-amazing-work-img-hover-description'>erwr</p></div>")
//     }
// })





// let ourServicesImages = document.querySelectorAll('.our-amazing-work-img');
//
// ourServicesImages.forEach((el) => {
//     el.addEventListener('mouseenter', (elem) => {
//
//         if (elem.target.nodeName === "IMG") {
//             console.log(elem);
//             elem.target.parentNode.insertAdjacentHTML("afterbegin", "<div class='our-amazing-work-img-hover'><div class='our-amazing-work-img-hover-link-wrap'><a class='our-amazing-work-img-hover-link' href='#'></a><a class='our-amazing-work-img-hover-link' href='#'></a></div><p class='our-amazing-work-img-hover-title'>creative design</p><p class='our-amazing-work-img-hover-description'>erwr</p></div>")
//         }
//     });
//
//     el.addEventListener('mouseleave', (e) => {
//         console.log(e.relatedTarget, 'out')
//     })
// });






let ourServicesLinks = document.querySelectorAll('.our-amazing-work-link');

ourServicesLinks.forEach(e => {
    e.addEventListener('mouseenter', el => {
        let desc = el.target.firstElementChild.getAttribute('alt');

        el.target.insertAdjacentHTML("afterbegin", "<div class='our-amazing-work-img-hover'><div class='our-amazing-work-img-hover-link-wrap'><a class='our-amazing-work-img-hover-link' href='#'></a><a class='our-amazing-work-img-hover-link' href='#'></a></div><p class='our-amazing-work-img-hover-title'>creative design</p><p class='our-amazing-work-img-hover-description'>" + desc + "</p></div>");
    });

    e.addEventListener('mouseleave', el => {
        el.target.firstElementChild.remove();
    });
});

new Glide('.glide', {
    type: 'carousel',
    autoplay: 0,
}).mount();






