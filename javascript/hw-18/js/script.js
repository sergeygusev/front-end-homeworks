"use strict"

function objectCopy(object1, object2) {
    for (let key in object2) {
        if (typeof object2[key] === "object") {
            objectCopy(object1, object2[key]);
        } else {
            object1[key] = object2[key];
        }
    }
}

let object1 = {
    name1: "name1",
    name2: "name2"
}

let object2 = {
    name3: "name3",
    name4: "name4",
    name5: {
        name6: "name6",
        name7: "name7"
    }
}

objectCopy(object1, object2);

console.log(object1);







