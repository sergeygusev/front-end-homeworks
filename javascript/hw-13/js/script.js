"use strict"

let changeTheme = document.querySelector('.change-theme');
let table = document.querySelector('.main-table');
let localTheme = localStorage.getItem('theme');

if (localTheme) {
    table.classList.add(localTheme);
}

changeTheme.addEventListener('click', () => {

    table.classList.toggle('new-theme');
    try {
        if (localTheme) {
            localStorage.removeItem("theme");
        } else {
            localStorage.setItem('theme', 'new-theme');
        }
    } catch (e) {
        if (e == QUOTA_EXCEEDED_ERR) {
            alert('Превышен лимит');
        }
    }

});







