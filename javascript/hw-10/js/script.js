"use strict"

event.preventDefault();

let icon = document.querySelectorAll('i');
let input = document.querySelectorAll('input');
let label = document.querySelectorAll('.input-wrapper');
let form = document.querySelector('.password-form');

function errorMessage(message) {
    let errorMessage = document.createElement('p');

    errorMessage.classList.add('error');
    errorMessage.innerText = message;

    label[1].after(errorMessage);
}

icon.forEach(el => {
    el.addEventListener('click', () => {
        el.classList.toggle('fa-eye-slash');
        if (el.classList.contains('fa-eye-slash')) {
            el.previousElementSibling.setAttribute('type', 'text');
        } else {
            el.previousElementSibling.setAttribute('type', 'password');
        }
    })
});

form.addEventListener('submit', () => {
    let error = document.querySelector('.error');

    if (error !== null)
        error.remove();

    if (input[0].value.length === 0 && input[1].value.length === 0) {
        errorMessage("Пустые пароли недопустимы");
        return false;
    }

    if (input[0].value === input[1].value) {
        setTimeout(() => {
            alert("You are welcome");
        }, 100);

    } else {
        errorMessage("Нужно ввести одинаковые значения");
    }
})









