"use strict"

function filterBy(array, type) {
    return array.filter(element => typeof element !== type);
}

let array = [
    'hello',
    'world',
    23,
    '23',
    null
];

console.log(filterBy(array, "string"));




