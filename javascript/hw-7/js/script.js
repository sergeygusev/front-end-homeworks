"use strict"

let listArray = [
    'hello',
    'world',
    'Kiev',
    'Kharkiv',
    'Odessa',
    'Lviv',
    '1',
    '2',
    '3',
    'sea',
    'user'
];

function createListFromArray(array) {

    let list = document.createElement('ul');
    let listArray = [];

    array.map(elem => {
        let liItem = document.createElement('li');

        liItem.innerHTML = elem;
        listArray.push(liItem);
    });

    listArray.forEach(value => {
        list.append(value);
    })

    document.body.append(list);

}

createListFromArray(listArray);

