"use strict"

let tab = document.querySelectorAll(".tabs-title");
let content = document.querySelector('.tabs-content');

tab.forEach((el, index) => {

    if (el.classList.contains('active')) {
        content.children[index].style.display = 'block';
    }

    el.addEventListener('click', event => {
        let active = document.querySelector(".active");

        active.classList.remove('active');
        event.target.classList.add('active');

        let contentItem = content.children;

        Array.from(contentItem).forEach(item => {
            let dataAttribute = el.innerText.toLowerCase();

            if (dataAttribute === item.dataset.attr) {
                item.style.display = 'block';
            } else
                item.style.display = 'none';
        })

    })

})




