"use strict"

let menu = document.querySelector(".menu");
let burger = document.querySelector(".menu__hamburger-wrap");
let menuList = document.querySelector(".menu__list");

menu.addEventListener('mousedown', elem => {
    console.log('test');
    if (elem.currentTarget.classList.contains('menu_active')) {
        menuList.style.display = "none";
        elem.currentTarget.classList.remove("menu_active");
        burger.style.display = 'block';
    } else {
        menuList.style.display = "block";
        elem.currentTarget.classList.add("menu_active");
        burger.style.display = 'none';
    }
})

