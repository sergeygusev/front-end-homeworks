import React, { Component } from 'react';

class Buttons extends Component{
    render() {
        const { text, backgroundColor, handler } = this.props;

        return (
            <button
                className="btn"
                style={{backgroundColor: backgroundColor}}
                onClick={handler}
            >
                {text}
            </button>
        )
    }
}

export default Buttons;