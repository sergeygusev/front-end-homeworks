import React from 'react';
import './App.scss';
import Buttons from './components/Buttons';
import Modal from "./components/Modal";

class App extends React.Component {
    state = {
        status: false
    }

    modalFirstHandler = () => {
        this.setState(() => ({
                statusFirst: true
            }
        ))
    }

    modalSecondHandler = () => {
        this.setState(() => ({
                statusSecond: true
            }
        ))
    }

    close = () => {
        this.setState(() => ({
                statusFirst: false,
                statusSecond: false,
            }
        ))
    }

    render() {
        return (
            <div className="container">
                {
                    this.state.statusFirst &&
                    <Modal
                        closeButton={true}
                        header="Do you want to delete this file?"
                        text="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"
                        close={this.close}
                        actions={(<div className="buttons"><button className="btn btn_first">Ok</button><button className="btn btn_first">Cancel</button></div>)}/>
                }
                {
                    this.state.statusSecond &&
                    <Modal
                        closeButton={true}
                        header="Are you sure?"
                        text="This file will be deleted"
                        close={this.close}
                        actions={(<div className="buttons"><button className="btn btn_second">Ok</button><button className="btn btn_second">Cancel</button></div>)}/>
                }
                <Buttons
                    text="Open first modal"
                    backgroundColor="lightred"
                    handler={this.modalFirstHandler}
                />
                <Buttons
                    text="Open second modal"
                    backgroundColor="lightblue"
                    handler={this.modalSecondHandler}
                />
            </div>
        )
    }
}

export default App;
