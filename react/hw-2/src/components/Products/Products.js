import React from "react";
import Product from "../Product/Product";
import './Products.scss';
import PropTypes from "prop-types";

class Products extends React.Component{
    render() {
        const { products, addToCart, addToFavorites, favorites, cart, removeFromCart } = this.props;

        return (
            <div className="products__container">
                {
                    products.map(el =>
                        <Product
                            key={el.id}
                            cart={cart}
                            favorites={favorites}
                            addToFavorites={addToFavorites}
                            addToCart={addToCart}
                            removeFromCart={removeFromCart}
                            data={el}
                        />)
                }
            </div>
        )
    }
}

Products.propTypes = {
    products: PropTypes.array.isRequired,
    addToCart: PropTypes.func.isRequired,
    addToFavorites: PropTypes.func.isRequired,
    favorites: PropTypes.array.isRequired,
    cart: PropTypes.array.isRequired,
    removeFromCart: PropTypes.func.isRequired
}

export default Products;