import React from "react";
import './Favorites.scss';
import PropTypes from "prop-types";

class Favorites extends React.Component {
    render() {
        const { favorites } = this.props;

        return (
            <h3>Избранное: {favorites.length}</h3>
        )
    }
}

Favorites.propTypes = {
    favorites: PropTypes.array.isRequired
}

export default Favorites;