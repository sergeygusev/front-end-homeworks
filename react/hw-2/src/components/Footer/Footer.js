import React from "react";
import footerBg from '../../img/footer.png';

class Footer extends React.Component {
    render() {
        return (
            <img
                src={footerBg}
                alt=""
            />
        )
    }
}

export default Footer;