import React from "react";
import './Cart.scss';

class Cart extends React.Component {
    render() {
        const { cart } = this.props;

        return (
            <h3>В корзине: {cart.length}</h3>
        )
    }
}

export default Cart;