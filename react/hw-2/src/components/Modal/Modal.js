import React, { Component, Fragment } from 'react';
import './Modal.scss';
import PropTypes from "prop-types";

class Modal extends Component {
    render() {
        const { closeButton, text, actions, close, header } = this.props;

        return (
            <Fragment>
                <div onClick={
                    (event) =>
                    event.target.children[0] ? event.target.children[0].classList.contains("popup") ? close() : null : ''
                } className="popup-wrap popup-wrap_active">
                    <div className="popup">
                        <div className="popup__header">
                            <h3 className="popup__caption">{header}</h3>
                            {closeButton && <div onClick={close} className="close"></div>}
                        </div>
                        <div className="popup__text">{text}</div>
                        {actions}
                    </div>
                </div>
            </Fragment>
        )
    }
}

Modal.propTypes = {
    closeButton: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired,
    actions: PropTypes.string.isRequired,
    close: PropTypes.func.isRequired,
    header: PropTypes.string.isRequired
}

export default Modal;