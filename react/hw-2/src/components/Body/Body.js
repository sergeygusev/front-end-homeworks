import React, { Fragment } from "react";
import axios from "axios";
import Loader from "../Loader/Loader";
import Products from "../Products/Products";
import PropTypes from "prop-types";

class Body extends React.Component {
    state = {
        products: [],
        isLoading: true
    }

    componentDidMount() {
        setTimeout(() => {
            axios('products.json')
                .then(res => this.setState({isLoading: false, products: res.data}));
        }, 1000);

    }

    render() {
        const { products, isLoading } = this.state;
        const { addToCart, addToFavorites, favorites, cart, removeFromCart } = this.props;

        if (isLoading) {
            return <Loader/>
        }

        return (
            <Fragment>
                <Products
                    cart={cart}
                    favorites={favorites}
                    addToFavorites={addToFavorites}
                    addToCart={addToCart}
                    removeFromCart={removeFromCart}
                    products={products}
                />
            </Fragment>
        )
    }
}

Body.propTypes = {
    addToCart: PropTypes.func.isRequired,
    addToFavorites: PropTypes.func.isRequired,
    favorites: PropTypes.array.isRequired,
    cart: PropTypes.array.isRequired,
    removeFromCart: PropTypes.func.isRequired
}

export default Body;