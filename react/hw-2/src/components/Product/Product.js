import React, { Fragment } from "react";
import './Product.scss';
import Button from "../Button/Button";
import Star from "../../icons/Star";
import PropTypes from "prop-types";

class Product extends React.Component{
    render() {
        const { data, addToCart, addToFavorites, favorites, cart, removeFromCart } = this.props;

        return (
            <Fragment>
                <div className="product__wrap">
                    <div className="product__image">
                        <img src={data.image} alt="" />
                    </div>
                    <div className="favorite">
                        <Star
                            color={favorites.includes(data.id) ? "yellow" : "white"}
                            onClick={addToFavorites}
                            productId={data.id}
                        />
                    </div>
                    <div className="product__title">{data.title}</div>
                    <div className="product__artikul"> Артикул: {data.artikul}</div>
                    <div className="product__bottom">
                        <div className="product__price">${data.price}</div>
                        <Button
                            className="product__add-to-cart"
                            text={cart.includes(data.id) ? "in cart" : "add to cart"}
                            backgroundColor={cart.includes(data.id) ? "yellow" : "black"}
                            color={cart.includes(data.id) ? "black" : "white"}
                            borderColor={cart.includes(data.id) ? "black" : "transparent"}
                            handler={cart.includes(data.id) ? removeFromCart : addToCart}
                            productId={data.id}
                        />
                    </div>
                </div>
            </Fragment>
        )
    }
}

Product.propTypes = {
    data: PropTypes.object.isRequired,
    addToCart: PropTypes.func.isRequired,
    addToFavorites: PropTypes.func.isRequired,
    favorites: PropTypes.array.isRequired,
    cart: PropTypes.array.isRequired,
    removeFromCart: PropTypes.func.isRequired
}

export default Product;