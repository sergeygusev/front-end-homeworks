import React, {Fragment} from 'react';
import headerBg from '../../img/upperHeader.png';
import quoteBg from '../../img/quote.png';
import Favorites from "../Favorites/Favorites";
import Cart from "../Cart/Cart";
import './Header.scss';
import PropTypes from "prop-types";

class Header extends React.Component {
    render() {
        const { favorites, cart } = this.props;

        return (
            <Fragment>
                <img src={headerBg} alt="" />
                <img src={quoteBg} alt="" />
                <div className="product__count">
                    <Favorites favorites={favorites}/>
                    <Cart cart={cart}/>
                </div>
            </Fragment>
        )
    }
}

Header.propTypes = {
    favorites: PropTypes.array.isRequired,
    cart: PropTypes.array.isRequired
}

export default Header;