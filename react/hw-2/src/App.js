import React from 'react';
import './App.scss';
import Button from './components/Button/Button';
import Modal from "./components/Modal/Modal";
import Header from "./components/Header/Header";
import Body from "./components/Body/Body";
import Footer from "./components/Footer/Footer";

class App extends React.Component {
    state = {
        status: false,
        favorites: JSON.parse(localStorage.getItem('favorites')) || [],
        cart: JSON.parse(localStorage.getItem('cart')) || []
    }

    modalHandler = (productId) => {
        this.setState(() => ({
                statusModal: 1,
                productId
            }
        ))
    };

    modalHandlerRemove = (productId) => {
        this.setState(() => ({
                statusModal: 2,
                productId
            }
        ))
    };

    close = () => {
        this.setState(() => ({
                statusModal: 0,
            }
        ))
    }

    toggleFavorites = (productId) => {
        const { favorites } = this.state;

        if (!favorites.includes(productId)) {
            this.setState({favorites: [...favorites, productId]}, () => {
                localStorage.setItem('favorites', JSON.stringify(this.state.favorites));
            });
        } else {
            this.setState({favorites: favorites.filter(el => el !== productId)}, () => {
                localStorage.setItem('favorites', JSON.stringify(this.state.favorites));
            });
        }
    }

    toggleCart = () => {
        const { cart, productId } = this.state;

        if (!cart.includes(productId)) {
            this.setState({cart: [...cart, productId]}, () => {
                localStorage.setItem('cart', JSON.stringify(this.state.cart));
            });
        } else {
            this.setState({cart: cart.filter(el => el !== productId)}, () => {
                localStorage.setItem('cart', JSON.stringify(this.state.cart));
            });
        }

        this.setState({statusModal: false});
    }

    render() {
        const { statusModal, favorites, cart } = this.state;

        return (
            <div className="app">
                <Header favorites={favorites} cart={cart}/>
                <div className="container">
                    <Body
                        addToFavorites={this.toggleFavorites}
                        addToCart={this.modalHandler}
                        removeFromCart={this.modalHandlerRemove}
                        favorites={favorites}
                        cart={cart}
                    />
                </div>
                <Footer/>
                {
                    statusModal === 1 ?
                    <Modal
                        closeButton={true}
                        header="Вы добавляете товар в корзину"
                        text="Вы уверены?"
                        close={this.close}
                        actions={(<div className="buttons">
                            <Button
                                className="btn btn_first"
                                text="Да"
                                handler={this.toggleCart}
                                backgroundColor="orange"
                                color="black"
                            />
                            <Button
                                handler={this.close}
                                className="btn btn_first"
                                text="Отмена"
                                backgroundColor="orange"
                                color="black"
                            />
                        </div>)}
                    /> :
                    (statusModal === 2 ?
                    <Modal
                        closeButton={true}
                        header="Вы удаляете товар из корзины"
                        text="Вы уверены?"
                        close={this.close}
                        actions={(<div className="buttons">
                            <Button
                                className="btn btn_first"
                                text="Да"
                                handler={this.toggleCart}
                            />
                            <Button
                                handler={this.close}
                                className="btn btn_first"
                                text="Отмена"
                            />
                        </div>)}
                    /> :
                    ''
                    )
                }
            </div>
        )
    }
}

export default App;
