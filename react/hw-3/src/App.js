import React, { useState, useEffect } from 'react';
import './App.scss';
import Button from './components/Button/Button';
import Modal from "./components/Modal/Modal";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import AppRoutes from "./routes/AppRoutes/AppRoutes";
import TopMenu from "./components/TopMenu/TopMenu";

const App = () => {
    const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem('favorites')) || []);
    const [cart, setCart] = useState(JSON.parse(localStorage.getItem('cart')) || []);
    const [statusModal, setStatusModal] = useState(false);
    const [productId, setproductId] = useState(null);

    const favoriteCount = (count) => {
        setFavorites(count);
    }

    const cartCount = (count) => {
        setCart(count);
    }

    const modalOpen = (productId) => {
        setproductId(productId);
        setStatusModal(true);
    };

    const close = () => {
        setStatusModal(false);
    }

    const removeFromCart = () => {
        if (cart.includes(productId)) {
            setCart(cart.filter(el => el !== productId))
        }

        setStatusModal(false);
    }

    const addToCart = (productId) => {
        if (!cart.includes(productId)) {
            setCart([...cart, productId])
        }
    }

    useEffect(() => {
        localStorage.setItem('cart', JSON.stringify(cart));
        cartCount(cart)
    }, [cart]);

    return (
        <div className="app">
            <Header/>
            <div className="container">
                <TopMenu
                    favorites={favorites.length}
                    cart={cart.length}
                />
                <AppRoutes
                    favoriteCount={favoriteCount}
                    cartCount={cartCount}
                    modalOpen={modalOpen}
                    cart={cart}
                    addToCart={addToCart}
                />
            </div>
            <Footer/>
            {
                statusModal &&
                <Modal
                    closeButton={true}
                    header="Вы удаляете товар из корзины"
                    text="Вы уверены?"
                    close={close}
                    actions={(<div className="buttons">
                        <Button
                            className="btn btn_first"
                            text="Да"
                            handler={removeFromCart}
                        />
                        <Button
                            handler={close}
                            className="btn btn_first"
                            text="Отмена"
                        />
                    </div>)}
                />
            }
        </div>
    )
}

export default App;
