import React, {Fragment} from 'react';
import headerBg from '../../img/upperHeader.png';
import quoteBg from '../../img/quote.png';
import './Header.scss';
import PropTypes from "prop-types";

const Header = () => {
    return (
        <Fragment>
            <img src={headerBg} alt="" />
            <img src={quoteBg} alt="" />
        </Fragment>
    )
}

Header.propTypes = {
    favorites: PropTypes.array.isRequired,
    cart: PropTypes.array.isRequired
}

export default Header;