import React, { Fragment } from 'react';
import './Modal.scss';
import PropTypes from "prop-types";

const Modal = ({ closeButton, text, actions, close, header }) => {
    return (
        <Fragment>
            <div onClick={
                (event) =>
                event.target.children[0] ? event.target.children[0].classList.contains("popup") ? close() : null : ''
            } className="popup-wrap popup-wrap_active">
                <div className="popup">
                    <div className="popup__header">
                        <h3 className="popup__caption">{header}</h3>
                        {closeButton && <div onClick={close} className="close"></div>}
                    </div>
                    <div className="popup__text">{text}</div>
                    {actions}
                </div>
            </div>
        </Fragment>
    )
}

Modal.propTypes = {
    closeButton: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.bool
    ]),
    text: PropTypes.string.isRequired,
    actions: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ]),
    close: PropTypes.func.isRequired,
    header: PropTypes.string.isRequired
}

Modal.defaultProps = {
    closeButton: null,
    actions: null
}

export default Modal;