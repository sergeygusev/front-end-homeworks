import React, { useState, useEffect, Fragment } from "react";
import axios from "axios";
import Loader from "../Loader/Loader";
import Products from "../Products/Products";
import PropTypes from "prop-types";

const Body = ({ addToCart, addToFavorites, favorites, cart, removeFromCart }) => {
    const [products, setProducts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            axios('products.json')
                .then(res => {setIsLoading(false); setProducts(res.data)})
        }, 1000);
    }, [setIsLoading]);

    if (isLoading) {
        return <Loader/>
    }

    return (
        <Fragment>
            <Products
                cart={cart}
                favorites={favorites}
                addToFavorites={addToFavorites}
                addToCart={addToCart}
                removeFromCart={removeFromCart}
                products={products}
            />
        </Fragment>
    )
}

Body.propTypes = {
    addToCart: PropTypes.func.isRequired,
    addToFavorites: PropTypes.func.isRequired,
    favorites: PropTypes.array.isRequired,
    cart: PropTypes.array.isRequired,
    removeFromCart: PropTypes.func.isRequired
}

export default Body;