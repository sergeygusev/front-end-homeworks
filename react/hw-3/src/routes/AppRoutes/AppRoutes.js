import React, { useState, useEffect } from "react";
import { Route, Switch } from "react-router-dom";
import FavoritesPage from "../../pages/FavoritesPage/FavoritesPage";
import CartPage from "../../pages/CartPage/CartPage";
import Body from "../../components/Body/Body";

const AppRoutes = ({ favoriteCount, cartCount, modalOpen, cart, addToCart }) => {
    const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem('favorites')) || []);
    // const [cart, setCart] = useState(JSON.parse(localStorage.getItem('cart')) || []);

    useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(favorites));
        favoriteCount(favorites);
    }, [favorites]);
    //
    // useEffect(() => {
    //     localStorage.setItem('cart', JSON.stringify(cart));
    //     cartCount(cart)
    // }, [cart]);

    const toggleFavorites = (productId) => {
        if (!favorites.includes(productId)) {
            setFavorites([...favorites, productId])
        } else {
            setFavorites(favorites.filter(el => el !== productId))
        }
    }

    // const addToCart = (productId) => {
    //     if (!cart.includes(productId)) {
    //         setCart([...cart, productId])
    //     }
    // }

    return (
        <>
            <Switch>
                <Route exact path="/" render={() =>
                    <Body
                        addToFavorites={toggleFavorites}
                        addToCart={addToCart}
                        favorites={favorites}
                        cart={cart}
                    />}
                />
                <Route path="/favorites" render={() =>
                    <FavoritesPage
                        favorites={favorites}
                        addToFavorites={toggleFavorites}
                    />}
                />
                <Route path="/cart" render={() =>
                    <CartPage
                        cart={cart}
                        modalOpen={modalOpen}
                    />}
                />
            </Switch>
        </>
    )
}

export default AppRoutes;