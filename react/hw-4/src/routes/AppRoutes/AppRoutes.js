import React, { Fragment } from "react";
import { Route, Switch } from "react-router-dom";
import FavoritesPage from "../../pages/FavoritesPage/FavoritesPage";
import CartPage from "../../pages/CartPage/CartPage";
import Body from "../../components/Body/Body";

const AppRoutes = ({ addToFavorites, addToCart, removeFromCart, favorites, cart }) => {
    return (
        <Fragment>
            <Switch>
                <Route exact path="/" render={() =>
                    <Body
                        addToFavorites={addToFavorites}
                        addToCart={addToCart}
                        removeFromCart={removeFromCart}
                        favorites={favorites}
                        cart={cart}
                    />}
                />
                <Route path="/favorites" render={() =>
                    <FavoritesPage
                        favorites={favorites}
                        addToFavorites={addToFavorites}
                    />}
                />
                <Route path="/cart" render={() =>
                    <CartPage
                        cart={cart}
                        removeFromCart={removeFromCart}
                    />}
                />
            </Switch>
        </Fragment>
    )
}

export default AppRoutes;