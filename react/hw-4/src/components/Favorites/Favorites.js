import React from "react";
import './Favorites.scss';
import PropTypes from "prop-types";

const Favorites = ({ favorites }) => {
    return (
        <>
            Избранное: {favorites.length}
        </>
    )
}

Favorites.propTypes = {
    favorites: PropTypes.array.isRequired
}

export default Favorites;