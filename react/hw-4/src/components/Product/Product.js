import React, { Fragment } from "react";
import './Product.scss';
import Button from "../Button/Button";
import Star from "../../icons/Star";
import Remove from "../../icons/Remove";
import PropTypes from "prop-types";

const Product = ({ data, addToCart, addToFavorites, favorites, cart, removeFromCart, isFavorites, isCart }) => {
    return (
        <Fragment>
            <div className="product__wrap">
                <div className="product__image">
                    <img src={data.image} alt="" />
                </div>
                <div className="favorite">
                    {
                        !isCart &&
                        <Star
                            color={favorites.includes(data.id) ? "yellow" : "white"}
                            onClick={addToFavorites}
                            productId={data.id}
                        />
                    }
                </div>
                <div className="remove">
                    {
                        isCart &&
                        <Remove
                            onClick={removeFromCart}
                            productId={data.id}
                        />
                    }
                </div>
                <div className="product__title">{data.title}</div>
                <div className="product__artikul"> Артикул: {data.artikul}</div>
                <div className="product__bottom">
                    <div className="product__price">${data.price}</div>
                    {
                        !isFavorites && !isCart &&
                        <Button
                            className="product__add-to-cart"
                            text={cart.includes(data.id) ? "in cart" : "add to cart"}
                            backgroundColor={cart.includes(data.id) ? "yellow" : "black"}
                            color={cart.includes(data.id) ? "black" : "white"}
                            borderColor={cart.includes(data.id) ? "black" : "transparent"}
                            handler={addToCart}
                            productId={data.id}
                        />
                    }
                </div>
            </div>
        </Fragment>
    )
}

Product.propTypes = {
    data: PropTypes.object.isRequired,
    addToCart: PropTypes.func,
    addToFavorites: PropTypes.func,
    favorites: PropTypes.array,
    cart: PropTypes.array,
    removeFromCart: PropTypes.func,
    isFavorites:PropTypes.bool,
    isCart:PropTypes.bool
}

Product.defaultProps = {
    addToCart: null,
    addToFavorites: null,
    favorites: null,
    cart: null,
    removeFromCart: null,
    isFavorites: null,
    isCart: null
}

export default Product;