import React from "react";
import footerBg from '../../img/footer.png';

const Footer = () => {
    return (
        <img
            src={footerBg}
            alt=""
        />
    )
}

export default Footer;