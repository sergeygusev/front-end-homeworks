import React from "react";
import "./TopMenu.scss";
import { NavLink } from "react-router-dom";
import Favorites from "../Favorites/Favorites";
import Cart from "../Cart/Cart";

const TopMenu = ({ favorites, cart }) => {
    return (
        <ul className="topmenu__nav">
            <li>
                <NavLink exact to="/" activeClassName="link__active">
                    Главная
                </NavLink>
            </li>
            <li>
                <NavLink to="/favorites" activeClassName="link__active">
                    <Favorites
                        favorites={favorites}
                    />
                </NavLink>
            </li>
            <li>
                <NavLink to="/cart" activeClassName="link__active">
                    <Cart
                        cart={cart}
                    />
                </NavLink>
            </li>
        </ul>
    )
}

export default TopMenu;