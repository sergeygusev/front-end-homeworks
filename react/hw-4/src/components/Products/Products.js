import React from "react";
import Product from "../Product/Product";
import './Products.scss';
import PropTypes from "prop-types";
import Button from "../Button/Button";

const Products = ({ products, addToCart, addToFavorites, favorites, cart, removeFromCart, isFavorites, isCart }) => {
    return (
        <div className="products__container">
            {
                products.map(el =>
                    <Product
                        key={el.id}
                        cart={cart}
                        favorites={favorites}
                        addToFavorites={addToFavorites}
                        addToCart={addToCart}
                        removeFromCart={removeFromCart}
                        data={el}
                        isFavorites={isFavorites}
                        isCart={isCart}
                    />)
            }
        </div>
    )
}

Products.propTypes = {
    products: PropTypes.array.isRequired,
    addToCart: PropTypes.func,
    addToFavorites: PropTypes.func,
    favorites: PropTypes.array,
    cart: PropTypes.array,
    removeFromCart: PropTypes.func
}

Button.defaultProps = {
    addToCart: null,
    addToFavorites: null,
    favorites: null,
    cart: null,
    removeFromCart: null,
    isFavorites: false,
    isCart: false
}

export default Products;