import React, { useState, useEffect } from 'react';
import './App.scss';
import Button from './components/Button/Button';
import Modal from "./components/Modal/Modal";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import AppRoutes from "./routes/AppRoutes/AppRoutes";
import TopMenu from "./components/TopMenu/TopMenu";

import { connect } from "react-redux";

const App = () => {
    const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem('favorites')) || []);
    const [cart, setCart] = useState(JSON.parse(localStorage.getItem('cart')) || []);
    const [statusModal, setStatusModal] = useState(false);
    const [productId, setproductId] = useState(null);

    const modalHandlerRemove = (productId) => {
        setproductId(productId);
        setStatusModal(true);
    };

    const close = () => {
        setStatusModal(false);
    }

    useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(favorites));
    }, [favorites]);

    useEffect(() => {
        localStorage.setItem('cart', JSON.stringify(cart));
    }, [cart]);

    const toggleFavorites = (productId) => {
        if (!favorites.includes(productId)) {
            setFavorites([...favorites, productId])
        } else {
            setFavorites(favorites.filter(el => el !== productId))
        }
    }

    const addToCart = (productId) => {
        if (!cart.includes(productId)) {
            setCart([...cart, productId])
        }
    }

    const removeFromCart = () => {
        if (cart.includes(productId)) {
            setCart(cart.filter(el => el !== productId))
        }

        setStatusModal(false);
    }

    return (
        <div className="app">
            <Header favorites={favorites} cart={cart}/>
            <div className="container">
                <TopMenu
                    favorites={favorites}
                    cart={cart}
                />
                <AppRoutes
                    addToFavorites={toggleFavorites}
                    addToCart={addToCart}
                    removeFromCart={modalHandlerRemove}
                    favorites={favorites}
                    cart={cart}
                />
            </div>
            <Footer/>
            {
                statusModal &&
                <Modal
                    closeButton={true}
                    header="Вы удаляете товар из корзины"
                    text="Вы уверены?"
                    close={close}
                    actions={(<div className="buttons">
                        <Button
                            className="btn btn_first"
                            text="Да"
                            handler={removeFromCart}
                        />
                        <Button
                            handler={close}
                            className="btn btn_first"
                            text="Отмена"
                        />
                    </div>)}
                />
            }
        </div>
    )
}

function mapStateToProps(state) {
    return {
        user: state.userInfo.user,
        code: state.userInfo.code
    }
}

export default connect(mapStateToProps)(App);
