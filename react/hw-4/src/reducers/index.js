import { combineReducers } from "redux";
import userInfo from "./main";

const rootReducer = combineReducers({
    userInfo
});

export default rootReducer;