import React, { useState, useEffect } from "react";
import './CartPage.scss';
import axios from "axios/index";
import Loader from "../../components/Loader/Loader";
import Products from "../../components/Products/Products";

const CartPage = ({ removeFromCart, cart }) => {
    const [products, setProducts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            axios('products.json')
                .then(res => {setIsLoading(false); setProducts(res.data)});
        }, 1000);
    }, [setIsLoading]);

    const cartProducts = products.filter(el => cart.includes(el.id));

    if (isLoading) {
        return <Loader/>
    }

    return (
        <>
            <Products
                cart={cart}
                removeFromCart={removeFromCart}
                products={cartProducts}
                isCart={true}
            />
        </>
    )
}

export default CartPage;