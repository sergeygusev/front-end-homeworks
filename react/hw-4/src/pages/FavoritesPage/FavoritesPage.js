import React, { useState, useEffect } from "react";
import './FavoritesPage.scss';
import axios from "axios/index";
import Loader from "../../components/Loader/Loader";
import Products from "../../components/Products/Products";

const FavoritesPage = ({ addToFavorites, favorites }) => {
    const [products, setProducts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            axios('products.json')
                .then(res => {setIsLoading(false); setProducts(res.data)});
        }, 1000);
    }, [setIsLoading]);

    const favoriteProducts = products.filter(el => favorites.includes(el.id));

    if (isLoading) {
        return <Loader/>
    }

    return (
        <>
            <Products
                favorites={favorites}
                addToFavorites={addToFavorites}
                products={favoriteProducts}
                isFavorites={true}
            />
        </>
    )
}

export default FavoritesPage;