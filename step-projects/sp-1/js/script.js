"use strict"

let serviceTab = document.querySelector(".our-services-tabs");
let ourAmazingTab = document.querySelector(".our-amazing-work-tabs");
let servicesContent = document.querySelector('.our-services-tabs-content');
let ourAmazingContent = document.querySelector('.our-amazing-work-tabs-content');
let loadMore = document.querySelector('.load-more');
let spinnerShow = document.querySelector('.our-amazing-work-tabs-content');
let tabItems = document.querySelectorAll('.our-services-tabs-content-item');

tabItems[0].style.display = 'block';

function tabChecker(tab, tabContent, tabActive) {
    tab.addEventListener('click', event => {
        let active = document.querySelector(`.${tabActive}`);
        let ourAmazingItems = document.querySelectorAll('.our-amazing-work-link');
        let dataAttribute = event.target.innerText.toLowerCase().replace(/ /g,"-");

        active.classList.remove(tabActive);
        event.target.classList.add(tabActive);

        if (tabActive !== 'work-active') {
            tabItems.forEach(function(el) {
                el.style.display = 'none';
            });
            document.querySelector(`[data-attr=${dataAttribute}]`).style.display = 'block';
        } else {
            ourAmazingItems.forEach(function(el) {
                if (dataAttribute === 'all') {
                    el.style.display = 'block';
                } else {
                    if (dataAttribute !== el.dataset.attr) {
                        el.style.display = 'none';
                    } else {
                        el.style.display = 'block';
                    }
                }
            });
        }
    })
}

tabChecker(serviceTab, servicesContent, 'active');
tabChecker(ourAmazingTab, ourAmazingContent, 'work-active');

function addElements() {
    let spinner = document.querySelector('.bar-wrapper');
    ourAmazingContent.insertAdjacentHTML("beforeend", ourAmazingContent.innerHTML);
    spinner.remove();
}

loadMore.addEventListener('click', el => {
    loadMore.remove();
    spinnerShow.insertAdjacentHTML("afterend", '<div class="bar-wrapper"><div class="bar"></div></div>')
    setTimeout(addElements, 2000);
})

new Glide('.glide', {
    type: 'carousel',
    autoplay: 0,
}).mount();






